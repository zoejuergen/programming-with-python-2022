
###################################################################
##DAY1##

# total_calorie = []
# with open("AoC01.txt", "rt") as myfile:
#     summe = 0
#     for line in myfile:
#         line = line.strip()
#         if line == "":
#             total_calorie.append(summe)
#             summe = 0
#             continue
#         summe += int(line)



# total_calorie.sort(reverse=True)
# the_elve = max(total_calorie)
# elve = (total_calorie.index(the_elve))+1
# top3 = sum(total_calorie[0:3])
# mytop3 = total_calorie[0] + total_calorie[1] + total_calorie[2]

# print(total_calorie, the_elve, elve, top3,mytop3)
# print(the_elve)
# print (elve)
# print(top3)
# print(mytop3)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# # def get_total_calorie(elve_list):
# #     elve = 0
# #     total_calorie = []
# #     for elve_items in elves_list:
# #         total_calorie.append(sum(elve_items))
# #         total_calorie.sort(reverse=True)
# #     return total_calorie

# # def pick_elve (x):
# #    # print(elves_list)
# #     elve = (becky.index(max(becky)))+1
# #     return elve

# # total_calorie = []
# # with open("AoC01.txt", "rt") as myfile:
# #     one_elve = []
# #     for line in myfile:
# #         line = line.strip()
# #         if line == "":
# #             total_calorie.append(one_elve)
# #             one_elve = []
# #             continue
# #         one_elve.append(int(line))
# # elves_list = total_calorie

# # becky = get_total_calorie(elves_list)
# # print(becky)
# # print (pick_elve(becky))

#############################################################################
##DAY2##
    ### übergeben wird direkt das generator-object durch split_lines
    ### (Generator muss nicht in global nicht im global scobe gespeichert werden )
    ### aber man muss es siche vorstellen als eine liste von objekten
    ### also sich dann nur eine line angucken

def split_lines():
    with open("AoC02.txt", "rt") as myfile:
        for line in myfile:
            yield line.strip()


def get_sum(split_lines):
    total_points=0
    for lines in split_lines:
        line_generator = str(lines)
        translate = line_generator.translate(_encoding)
        total_points += int(translate[2])
        if (lines[0]=="A" and lines[2]=="Y") or (lines[0]=="B" and lines[2]=="Z") or (lines[0]=="C" and lines[2]=="X"):
            total_points+=6
        elif ((lines[0]=="A" and lines[2]=="X") or (lines[0]=="B" and lines[2]=="Y") or (lines[0]=="C" and lines[2]=="Z")):
            total_points+=3
    return total_points


# _encoding = maketrans("XYZ","123")
# line_generator = ["A Y","B X", "C Z"]
_encoding = str.maketrans("XYZ","123")
print(get_sum(split_lines()))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def split_lines():
    with open("AoC02.txt", "rt") as myfile:
        for line in myfile:
            yield line.strip()


def get_sum(split_lines):
    counter = 0
    for lines in split_lines:
        if lines[2] == "X": #lose
            ####counter+=0
            if lines[0] == "A":
                counter += 3
            if lines[0] == "B":
                counter += 1
            if lines[0] == "C":
                counter += 2
        elif lines[2] == "Y": #draw
            counter += 3
            if lines[0] == "A":
                counter += 1
            if lines[0] == "B":
                counter += 2
            if lines[0] == "C":
                counter += 3
        elif lines[2] == "Z": #win
            counter += 6
            if lines[0] == "A":
                counter += 2
            if lines[0] == "B":
                counter += 3
            if lines[0] == "C":
                counter += 1
    return counter


print(get_sum(split_lines()))


############################################################################
###DAY3
###Part1

def split_lines():
    with open("AoC02.txt", "rt") as myfile:
        for line in myfile:
            return line.strip().split()



print(split_lines())