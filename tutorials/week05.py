
# #part 1
# def valid_passphrase(words):
#     for i in range(len(words)):
#         first_word=words[i]
#         for j in range(i+1, len(words)):
#             second_word=words[j]
#             if first_word==second_word:
#                 return False

#     return True

# with open("input04.txt", "rt") as myfile:
#     counter=0
#     for line in myfile:
#         words = line.split()
#         if valid_passphrase(words):
#             counter+=1
#     print(counter)


# #part1 with generator

# def split_lines():
#     with open("input04.txt", "rt") as myfile:
#         for line in myfile:
#             yield line.split()

# def valid_passphrase(words):
#     for i in range(len(words)):
#         first_word=words[i]
#         for j in range(i+1, len(words)):
#             second_word=words[j]
#             if first_word==second_word:
#                 return False

#     return True

# counter=0
# line_generator=split_lines()
# for lines in line_generator:
#     if valid_passphrase(lines):
#         counter+=1
# print(counter)



# #part1 with generator + set

# def split_lines():
#     with open("input04.txt", "rt") as myfile:
#         for line in myfile:
#             yield line.split()

# def valid_passphrase(words):
#     return (len(words)==len(set(words)))
# # def valid_passphrase(words):
# #     checked_word = set()
# #     for i in words:
# #         checked_word.add(i)
# #     return (len(words)==len(checked_word))


# counter=0
# line_generator=split_lines()
# for lines in line_generator:
#     if valid_passphrase(lines):
#         counter+=1
# print(counter)

#part2

def split_lines():
    with open("input04.txt", "rt") as myfile:
        for line in myfile:
            yield line.split()


def valid_passphrase(words):
    checked_word = set()
    for word in words:
        sorted_word=sorted(word)
        checked_word.add("".join(sorted_word))

    return (len(words)==len(checked_word))


counter=0
line_generator=split_lines()
for lines in line_generator:
    if valid_passphrase(lines):
        counter+=1
print(counter)


# #part2

# def split_lines():
#     with open("input04.txt", "rt") as myfile:
#         for line in myfile:
#             yield line.split()


# def valid_passphrase(words):
#     checked_word = set()
#     checked_letter= set()
#     for word in words:
#         for letter in word:
#             checked_letter.add(letter)
#             if len(word)==len(checked_letter):
#                 return True
#         checked_word.add(frozenset(checked_letter))

#     return (len(words)==len(checked_word))


# counter=0
# line_generator=split_lines()
# for lines in line_generator:
#     if valid_passphrase(lines):
#         counter+=1
# print(counter)




# # im trying sum -- dont like it

# def split_lines():
#     with open("input04.txt", "rt") as myfile:
#         for line in myfile:
#             yield line.split()


# def valid_passphrase(words):
#     checked_word = set()
#     checked_letter= set()
#     for word in words:
#         alphabet="abcdefghijklmnopqrstuvwxzy"
#         for i in range (len(word)):
#             letter=word[i]
#             if letter in alphabet:
#                 checked_letter.add(sum(alphabet[i]))

#         checked_word.add(frozenset(checked_letter))

#     return (len(words)==len(checked_word))


# counter=0
# line_generator=split_lines()
# for lines in line_generator:
#     if valid_passphrase(lines):
#         counter+=1
# print(counter)





# #beispiel
# def dna_kmers(k):
#     for ktup in itertools.product("ACGT",repeat=k):
#         yield "".join(ktup)

# def num_palindromes(kmers):
#     return sum(1 for kmer in kmers if reverscomp(kmer)==kmer)

# for k in range(13):
#     print (k, num_palindromes(dna_kmers(k)))

# #test generator

# def triangular_func()
# #part 2
# def valid_passphrase(words):
#     for i in range(len(words)):
#         first_word=words[i]
#         for j in range(i+1, len(words)):
#             second_word=words[j]
#             if first_word==second_word:
#                 return False
#             if len(first_word)==len(second_word):
#                 letters_dict={}
#                 for a_letter in first_word:
#                     letters_dict[next_letter]=0
#                 is_anagram=True
#                 for a_letter in second_word:
#                     if a_letter

#     return True

# with open("input04.txt", "rt") as myfile:
#     counter=0
#     for line in myfile:
#         words = line.split()
#         if valid_passphrase(words):
#             counter+=1
#     print(counter)
