# #############################################################
# #NORMAL KEY VS. LAMBDA
# def modulo7(x):
#         modulo = x%7
#         return modulo


# def sorted_mod_7 (liste):
#     liste.sort(key=modulo7, reverse=True)


# liste = [5, 48, 98]
# l = [521, 421, 9821, 7, 8]
# sorted_mod_7(liste)
# l.sort(key = lambda x: x%7, reverse=True)
# print(liste)
# print(l)

# def test_54898():
#         assert sorted_mod_7([5, 48, 98]) == [48, 5, 98]


#############################################################
#MAP VS. LIST COMPREHENSION

with open ("06-numbers.txt","rt") as myfile:
    data = myfile.read().split()


int_list = [int(x) for x in data] #list coprehension for list --> int_list
new_list = list(map(int, data)) # map(func, iterable) (int ist schon eine funktion)
print(int_list)
print(new_list)
# print(line)
# print(int_list)













