# #warm up 1
# from collections import Counter


# def duplicates (string):
#     duplicates_counter = Counter(string.lower())
#     print(duplicates_counter["a"])
#     duplicates_values=duplicates_counter.values()
#     counter=0
#     for i in duplicates_values:
#         if i > 1:
#             counter+=1
#     return (counter)


# print(duplicates("sakfjpdisgqs"))
# print(duplicates("aaBbccgspkenl"))

# def test_aabcedf():
#     assert(duplicates("aabcdef"))==1
# def test_aabbccddeeff():
#     assert(duplicates("AaBbCcDdEeFf"))==6
# def test_abcabcdef():
#     assert(duplicates("abcabcdef"))==3

# warm up 2


# def persistence (n):
#     number = int(n)
#     product = 1
#     persistence = 0
#     while number > 9:
#         for num in range(0,len(str(number))):
#             product*=int(str(number)[num])
#         persistence += 1
#         number = product
#         product = 1
#     return(persistence)

# print(persistence(999))

# def test_999():
#     assert persistence(999)==4
# def test_39():
#     assert persistence(39)==3

# #3 counting
# dna="ATGGCTTTCTTGAAGAGTTTCCCATTCTACGCTTTCTTGTGCTTCGGACAATACTTCGTG\
# GCTGTGACTCACGCAGACAACTTCCCATGCTCTAAGTTGACCAACAGGACCATCGGTAAT\
# CAATGGAACTTGATCGAGACCTTCTTGTTGAACTACTCATCTAGGTTGCCACCAAACTCT\
# GACGACGTGTTGGGTGACTACTTCCCAACTGTGCAACCTTGGTTCAACTGCATCAGGAAC\
# AACTCTAACGACTTGTACGTGACTTTGGAGAACTTGAAGGCTCTCTACTGGGACTACGCT\
# ACTGAGAACATCACCTGGAACCACAGGCAAAGGTTGAACGTGGTGGTGAACGGATACCCA\
# TACAGTATCACAGTGACAACAACCCGCAACTTCAACTCTGCTGAGGGTGCTATTATCTGC\
# ATTTGCAAGGGAAGTCCACCAACTACCACCACCGAGTCTAGTTTGACTTGCAACTGGGGA\
# AGTGAGTGCAGGTTGAACCACAAGTTCCCTATCTGTCCATCTAACTCAGAGGCAAACTGC\
# GGAAACATGCTGTACGGCTTGCAATGGTTCGCAGACGAGGTGGTGGCTTACTTGCATGGA\
# GCTAGTTACCGGATTAGTTTCGAGAACCAATGGTCTGGCACTGTGACATTCGGTGACATG\
# CGGGCCACAACATTGGAGGTGGCTGGCACGTTGGTGGACTTGTGGTGGTTCAACCCAGTG\
# TACGATGTCAGTTACTACAGGGTGAACAACAAGAACGGGACTACCGTGGTGAGCAACTGC\
# ACTGACCAATGCGCTAGTTACGTGGCTAACGTGTTCACTACACAGCCAGGAGGATTCATC\
# CCATCAGACTTTAGTTTCAACAACTGGTTCCTCTTGACTAACAGCAGCACTTTGGTGAGT\
# GGTAAGTTGGTGACCAAGCAGCCGTTGCTCGTTAACTGCTTGTGGCCAGTCCCAAGCTTC\
# GAGGAGGCAGCTTCTACATTCTGCTTCGAGGGAGCTGGCTTCGACCAATGCAATGGAGCT\
# GTGCTCAACAATACTGTGGACGTGATTAGGTTCAACCTCAACTTCACTACAAACGTGCAA\
# TCAGGGAAGGGTGCCACAGTGTTCTCATTGAACACAACCGGTGGAGTCACTCTCGAGATT\
# TCATGCTACACAGTGAGTGACTCGAGCTTCTTCAGTTACGGAGAGATTCCGTTCGGCGTG\
# ACTGACGGACCACGGTACTGCTACGTGCACTACAACGGCACAGCTCTCAAGTACCTCGGA\
# ACACTCCCACCTAGTGTGAAGGAGATTGCTATCAGTAAGTGGGGCCACTTCTACATTAAC\
# GGTTACAACTTCTTCAGCACATTCCCAATTGACTGCATCTCATTCAACTTGACCACTGGT\
# GACAGTGACGTGTTCTGGACAATCGCTTACACAAGCTACACTGAGGCACTCGTGCAAGTT\
# GAGAACACAGCTATTACAAAGGTGACGTACTGCAACAGTCACGTTAACAACATTAAGTGC\
# TCTCAAATTACTGCTAACTTGAACAACGGATTCTACCCTGTTTCTTCAAGTGAGGTTGGA\
# CTCGTGAACAAGAGTGTTGTGCTCCTCCCAAGCTTCTACACACACACCATTGTGAACATC\
# ACTATTGGGCTCGGAATGAAGCGTAGTGGGTACGGGCAACCAATCGCCTCAACATTGAGT\
# AACATCACATTGCCAATGCAGGACCACAACACCGATGTGTACTGCATTCGGTCTGACCAA\
# TTCTCAGTTTACGTGCATTCTACTTGCAAGAGTGCTTTGTGGGACAATATTTTCAAGCGA\
# AACTGCACGGACCACCACCATCACCATCACTAA"

# from collections import Counter

# def gc_content ():
#     bases= Counter(dna)
#     GC=bases["G"]+ bases["C"]
#     print("GC-content: " + str(int(GC/len(dna)*100)) + "%")

# gc_content()



#4 kmers



# def list_kmers(s,k):
#     if k>len(s):
#         return []
#     kmers_list= []
#     for i in range(0, len(s)):
#         if i+k > len(s):
#             break
#         kmers_list.append(s[i:i+(k)])
#     return kmers_list


# def number_unique_kmers (kmers_list):
#     unique_kmers= set()
#     for i in kmers_list:
#         unique_kmers.add(i)
#     print(len(kmers_list))
#     return(counter_unique_kmers:= len(kmers_list)-(len(kmers_list)-len(unique_kmers)))

# # print(number_unique_kmers(list_kmers(dna,6)))

# def test_ACGTGGTACGGATAG4():
#     assert(number_unique_kmers(list_kmers("ACGTGGTACGGATAG",4))) == 12
# def test_abbaabba4():
#     assert (number_unique_kmers(list_kmers("abbaabba",4)))== 4

# #5 kmer code


_encoding = str.maketrans("ACGT","0123")
def kmer_code (kmer):
    kmer_numbers = (kmer.translate(_encoding))
    code = 0
    n = len(kmer_numbers)-1
    for i in range(len(kmer_numbers)):
        base = kmer_numbers[i]
        code += int(base)*(4**n)
        n-=1
    return(code)

print(kmer_code("AACCG"))
def test_ACGT():
    assert(kmer_code("ACGT"))==27

def test_AACCG():
    assert(kmer_code("AACCG"))==22

def test_CCCC():
    assert(kmer_code("CCCC"))==85

#6 canonical code

_translationtable = str.maketrans("ACGT","TGCA")
def revcomp(somedna):
    return somedna.translate(_translationtable)[::-1]

# def test_ACGGATCGAT():
#     assert(revcomp("ACGGATCGAT"))=="ATCGATCCGT"


# # def canonical_code(kmer):
# def canical_code(kmer):


#     _translationtable = str.maketrans("ACGT","TGCA")
#     def revcomp(somedna):
#         complement= somedna.translate(_translationtable)[::-1]
#         return(complement)

#     _encoding = str.maketrans("ACGT","0123")
#     def kmer_code (kmer):
#         kmer_numbers = (kmer.translate(_encoding))
#         code = 0
#         n = len(kmer_numbers)-1
#         for i in range(len(kmer_numbers)):
#             base = kmer_numbers[i]
#             code += int(base)*(4**n)
#             n-=1
#         print(code)
#         return(code)

#     _encoding = str.maketrans("ACGT","0123")
#     def kmer_code (complement):
#         kmer_numbers = (complement.translate(_encoding))
#         code_refcomp = 0
#         n = len(kmer_numbers)-1
#         for i in range(len(kmer_numbers)):
#             base = kmer_numbers[i]
#             code_refcomp += int(base)*(4**n)
#             n-=1
#         print(code_refcomp)
#         return(code_refcomp)

#     if code > code_refcomp:
#         return(code)
#     return(code_refcomp)

# print(canical_code("ACGT"))

